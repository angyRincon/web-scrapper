const puppeteer = require("puppeteer")

async function whatScrap() {
    const browser = await puppeteer.launch({
        headless: false,
        args: ["--disable-setuid-sandbox"],
        'ignoreHTTPSErrors': true
    })

    const page = await browser.newPage();

    // Login
    await page.goto('https://web.whatsapp.com/', {waitUntil: "networkidle2", timeout: 50000});

    await page.waitForTimeout(10000)

    // Search
    await page.type("._2_1wd.copyable-text.selectable-text", "Edwin Grow Data")

    // Select Contact
    await page.waitForSelector(".TbtXF", {visible: true})
    await Promise.all([
        page.waitForTimeout(2000),
        page.click(".TbtXF")
    ])

    // Type a Message
    await page.type("._2A8P4", "  Hola jejeje")

    // Send a Message
    await page.waitForSelector("._1E0Oz", {visible: true})
    await Promise.all([
        page.waitForNavigation({waitUntil: "networkidle2"}),
        page.click("._1E0Oz")
    ])

    await page.waitForTimeout(40000)
    await page.close()
}

module.exports = () => whatScrap()
