const mongoose = require("mongoose")

const Facebook = mongoose.Schema({
    likes: Array,
    love: Array,
    care: Array,
    fun: Array,
    surprise: Array,
    sad: Array,
    angry: Array
}, {timestamps: true, versionKey: false})

module.exports = mongoose.model('Facebook', Facebook)
