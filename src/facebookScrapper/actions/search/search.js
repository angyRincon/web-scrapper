const search = async (page) => {
    await page.mouse.click(0, 0)
    await page.waitForSelector("input[type=search]", {visible: true})
    await page.mouse.click(0, 0)
    await page.type("input[type=search]", "Claudia López")
    await page.waitForSelector(".d2edcug0.hpfvmrgz.qv66sw1b.c1et5uql.rrkovp55.a8c37x1j.keod5gw0.nxhoafnm.aigsh9s9.d3f4x2em.fe6kdd0r.mau55g9w.c8b282yb.iv3no6db.jq4qci2q.a3bd9o3v.knj5qynh.oo9gr5id", {visible: true})
    await Promise.all([
        page.waitForNavigation({waitUntil: "networkidle2"}),
        page.click(".d2edcug0.hpfvmrgz.qv66sw1b.c1et5uql.rrkovp55.a8c37x1j.keod5gw0.nxhoafnm.aigsh9s9.d3f4x2em.fe6kdd0r.mau55g9w.c8b282yb.iv3no6db.jq4qci2q.a3bd9o3v.knj5qynh.oo9gr5id"),
    ])
}

module.exports = (page) => search(page)
