const login = require("./login/login")
const search = require("./search/search")
const selectPost = require("./posts/posts")
const filterComments = require("./comments/comments")
const scrollPage = require("./people/scroll")

module.exports = {
    login,
    search,
    selectPost,
    filterComments,
    scrollPage
}
