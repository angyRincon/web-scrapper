const positiveWords = require("../../../words/positiveWords");

const filterComments = async (page) => {
    await page.waitForSelector(".kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.c1et5uql", {visible: true})
    return await page.$$eval(".kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.c1et5uql", txt => {
        return txt.map(text => text.textContent)
    })
}

const filterByPositive = async (cleanElements) => {
    const filterByPositive = cleanElements.map(word => {
        const positive = positiveWords.map(positive => {
            const result = word.includes(positive) ? word : ''
            const value = result ? result : 'negativas'
            if (value === result) return value
        })
        return positive.find(a => a)
    })
    return filterByPositive.filter(a => a !== undefined)
}

module.exports = {
    filterComments,
    filterByPositive
}
