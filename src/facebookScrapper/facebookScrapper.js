const puppeteer = require("puppeteer")
const actions = require("./actions/index")
const submitAction = require("../general")
const Facebook = require("./models/Facebook")

async function facebookScrapper() {
    const browser = await puppeteer.launch({
        headless: false,
        args: ["--disable-setuid-sandbox"],
        'ignoreHTTPSErrors': true
    })

    let likes = []
    let love = []
    let care = []
    let fun = []
    let angry = []

    const page = await browser.newPage();

    // Login
    await actions.login(page)
    await submitAction(page)

    // Search
    await actions.search(page)

    // Select person
    await page.waitForSelector(".oajrlxb2.g5ia77u1.qu0x051f.esr5mh6w.e9989ue4.r7d6kgcz.rq0escxv.nhd2j8a9.nc684nl6.p7hjln8o.kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.jb3vyjys.rz4wbd8a.qt6c0cv9.a8nywdso.i1ao9s8h.esuyzwwr.f1sip0of.lzcic4wl.oo9gr5id.gpro0wi8.lrazzd5p.dkezsu63", {visible: true})
    await page.$$eval(".oajrlxb2.g5ia77u1.qu0x051f.esr5mh6w.e9989ue4.r7d6kgcz.rq0escxv.nhd2j8a9.nc684nl6.p7hjln8o.kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.jb3vyjys.rz4wbd8a.qt6c0cv9.a8nywdso.i1ao9s8h.esuyzwwr.f1sip0of.lzcic4wl.oo9gr5id.gpro0wi8.lrazzd5p.dkezsu63", txt => {
        return txt.map(data => data.attributes["aria-label"].value.includes('Cuenta verificada') ? data.click() : '')
    })

    // Select Post
    await actions.selectPost(page)

    // Open Reactions and total emotions
    await page.reload()
    await page.mouse.click(0, 0)
    await page.waitForSelector(".j1lvzwm4", {visible: true})
    await page.click(".j1lvzwm4")

    // Open Modal
    await page.waitForSelector(".j1lvzwm4", {visible: true})
    await page.mouse.click(0, 0)
    await Promise.all([
        page.waitForTimeout(3000),
        page.click(".j1lvzwm4"),
    ])

    await page.waitForTimeout(1000)

    // GET LIKES
    const quantity = await page.$eval(".d2edcug0.hpfvmrgz.qv66sw1b.c1et5uql.rrkovp55.a8c37x1j.keod5gw0.nxhoafnm.aigsh9s9.d3f4x2em.fe6kdd0r.mau55g9w.c8b282yb.iv3no6db.jq4qci2q.a3bd9o3v.lrazzd5p.q66pz984", txt => txt.textContent)
    const total = (parseInt(quantity) * 2) / 20
    await page.mouse.move(300, 150)
    for (let i = 0; i < total; i++) {
        await page.waitForTimeout(2000)
        await page.mouse.wheel({deltaY: 10000})
    }

    await page.waitForTimeout(3000)

    const likesUrls = await page.$$eval(".gs1a9yip.ow4ym5g4.auili1gw.rq0escxv.j83agx80.cbu4d94t.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.tgvbjcpo.hpfvmrgz.rz4wbd8a.a8nywdso.l9j0dhe7.du4w35lb.rj1gh0hx.pybr56ya.f10w8fjw > div > div > div > span > div > a", txt => {
        return txt.map(txt => txt.href)
    })

    console.log("LIKEEESS ", likesUrls)

    // GET LOVES
    await page.$$eval(".bp9cbjyn.rq0escxv.j83agx80.pfnyh3mw.cehpxlet.l9j0dhe7.aodizinl.hv4rvrfc.ofv0k9yr.dati1w0a", txt => {
        return txt[3].click()
    })

    const quantityLove = await page.$$eval(".d2edcug0.hpfvmrgz.qv66sw1b.c1et5uql.rrkovp55.a8c37x1j.keod5gw0.nxhoafnm.aigsh9s9.d3f4x2em.fe6kdd0r.mau55g9w.c8b282yb.iv3no6db.jq4qci2q.a3bd9o3v.lrazzd5p.m9osqain", txt => {
        return txt[7].textContent
    })
    const totalLove = (parseInt(quantityLove) * 2) / 20

    await page.mouse.move(300, 150)
    for (let i = 0; i < totalLove; i++) {
        await page.waitForTimeout(2000)
        await page.mouse.wheel({deltaY: 10000})
    }

    const loveUrls = await page.$$eval(".gs1a9yip.ow4ym5g4.auili1gw.rq0escxv.j83agx80.cbu4d94t.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.tgvbjcpo.hpfvmrgz.rz4wbd8a.a8nywdso.l9j0dhe7.du4w35lb.rj1gh0hx.pybr56ya.f10w8fjw > div > div > div > span > div > a", txt => {
        return txt.map(txt => txt.href)
    })
    console.log("LOOOVEEE ", loveUrls)

    // GET FUN
    await page.$$eval(".bp9cbjyn.rq0escxv.j83agx80.pfnyh3mw.cehpxlet.l9j0dhe7.aodizinl.hv4rvrfc.ofv0k9yr.dati1w0a", txt => {
        return txt[4].click()
    })
    await page.waitForTimeout(2000)

    const quantityFun = await page.$$eval(".d2edcug0.hpfvmrgz.qv66sw1b.c1et5uql.rrkovp55.a8c37x1j.keod5gw0.nxhoafnm.aigsh9s9.d3f4x2em.fe6kdd0r.mau55g9w.c8b282yb.iv3no6db.jq4qci2q.a3bd9o3v.lrazzd5p.m9osqain", txt => {
        return txt[8].textContent
    })
    const totalFun = (parseInt(quantityFun) * 2) / 20
    console.log(totalFun)

    await page.mouse.move(300, 150)
    for (let i = 0; i < totalFun + 1; i++) {
        await page.waitForTimeout(2000)
        await page.mouse.wheel({deltaY: 10000})
    }

    const funUrls = await page.$$eval(".gs1a9yip.ow4ym5g4.auili1gw.rq0escxv.j83agx80.cbu4d94t.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.tgvbjcpo.hpfvmrgz.rz4wbd8a.a8nywdso.l9j0dhe7.du4w35lb.rj1gh0hx.pybr56ya.f10w8fjw > div > div > div > span > div > a", txt => {
        return txt.map(txt => txt.href)
    })

    console.log("FUUUUNN ", funUrls)

    // GET CARE
    await page.$$eval(".bp9cbjyn.rq0escxv.j83agx80.pfnyh3mw.cehpxlet.l9j0dhe7.aodizinl.hv4rvrfc.ofv0k9yr.dati1w0a", txt => {
        return txt[5].click()
    })
    await page.waitForTimeout(2000)

    const careUrls = await page.$$eval(".gs1a9yip.ow4ym5g4.auili1gw.rq0escxv.j83agx80.cbu4d94t.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.tgvbjcpo.hpfvmrgz.rz4wbd8a.a8nywdso.l9j0dhe7.du4w35lb.rj1gh0hx.pybr56ya.f10w8fjw > div > div > div > span > div > a", txt => {
        return txt.map(txt => txt.href)
    })
    console.log("CAREEEE ", careUrls)

    // GET ANGRY
    await page.$$eval(".bp9cbjyn.rq0escxv.j83agx80.pfnyh3mw.cehpxlet.l9j0dhe7.aodizinl.hv4rvrfc.ofv0k9yr.dati1w0a", txt => {
        return txt[6].click()
    })
    await page.waitForTimeout(2000)

    const angryUrls = await page.$$eval(".gs1a9yip.ow4ym5g4.auili1gw.rq0escxv.j83agx80.cbu4d94t.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.tgvbjcpo.hpfvmrgz.rz4wbd8a.a8nywdso.l9j0dhe7.du4w35lb.rj1gh0hx.pybr56ya.f10w8fjw > div > div > div > span > div > a", txt => {
        return txt.map(txt => txt.href)
    })
    console.log("ANGRYYYYY ", angryUrls)

    let pagePromise = (link) => new Promise(async (resolve, reject) => {
        let information = {}
        await page.goto(link);

        await page.waitForTimeout(2000)
        await page.mouse.click(15, 20, {button: "left", clickCount: 2})

        // Click on information button
        await page.waitForSelector(".bp9cbjyn.rq0escxv.j83agx80.pfnyh3mw.frgo5egb.l9j0dhe7.cb02d2ww.hv4rvrfc.dati1w0a", {visible: true})
        await page.$$eval(".bp9cbjyn.rq0escxv.j83agx80.pfnyh3mw.frgo5egb.l9j0dhe7.cb02d2ww.hv4rvrfc.dati1w0a", txt => {
            return txt[2].click()
        })
        await page.waitForTimeout(2000)
        await page.mouse.click(15, 20, {button: "left", clickCount: 2})


        const name = await page.$eval(".gmql0nx0.l94mrbxd.p1ri9a11.lzcic4wl.bp9cbjyn.j83agx80", txt => txt.textContent)
        const living = await page.$$eval(".rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.jifvfom9.gs1a9yip.owycx6da.btwxx1t3.jb3vyjys.b5q2rw42.lq239pai.mysgfdmx.hddg9phg", txt => txt[2].textContent)

        await page.$$eval(".d2edcug0.hpfvmrgz.qv66sw1b.c1et5uql.rrkovp55.a8c37x1j.keod5gw0.nxhoafnm.aigsh9s9.d3f4x2em.fe6kdd0r.mau55g9w.c8b282yb.iv3no6db.jq4qci2q.a3bd9o3v.lrazzd5p.m9osqain", txt => {
            return txt[8].textContent === 'Lugares de residencia' ? txt[9].click() : txt[8].click()
        })

        await page.waitForTimeout(2000)

        const contact = await page.$$eval(".rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.jifvfom9.gs1a9yip.owycx6da.btwxx1t3.jb3vyjys.b5q2rw42.lq239pai.mysgfdmx.hddg9phg", txt => txt[0].textContent)
        const socialMedia = await page.$$eval(".rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.jifvfom9.gs1a9yip.owycx6da.btwxx1t3.jb3vyjys.b5q2rw42.lq239pai.mysgfdmx.hddg9phg", txt => txt[1].textContent)
        const genre = await page.$$eval(".rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.jifvfom9.gs1a9yip.owycx6da.btwxx1t3.jb3vyjys.b5q2rw42.lq239pai.mysgfdmx.hddg9phg", txt => txt[2].textContent)

        information['nombre'] = name
        information['residencia'] = living
        information['contact'] = contact
        information['socialMedia'] = socialMedia
        information['genre'] = genre

        resolve(information);
        console.log(information)
    })

    // for likes
    for (let link in likesUrls) {
        let currentPageData = await pagePromise(likesUrls[link]);
        likes.push(currentPageData)
    }

    await page.waitForTimeout(2000)

    // for love
    for (let link in loveUrls) {
        let currentPageData = await pagePromise(loveUrls[link]);
        love.push(currentPageData)
    }

    await page.waitForTimeout(2000)

    // for care
    for (let link in careUrls) {
        let currentPageData = await pagePromise(careUrls[link]);
        care.push(currentPageData)
    }

    await page.waitForTimeout(2000)

    // for fun
    for (let link in funUrls) {
        let currentPageData = await pagePromise(funUrls[link]);
        fun.push(currentPageData)
    }
    await page.waitForTimeout(2000)

    // for angry
    for (let link in angryUrls) {
        let currentPageData = await pagePromise(angryUrls[link]);
        angry.push(currentPageData)
    }

    console.log("LIKEESS ", likes)
    console.log("LOVEESS ", love)
    console.log("CAREEEE ", care)
    console.log("FUUUUUN ", fun)
    console.log("ANGRYYY ", angry)

    //Save into Database
    const newData = new Facebook({
        likes: likes,
        love: love,
        care: care,
        fun: fun,
        surprise: [],
        sad: [],
        angry: angry
    })

    await newData.save()

    return [likes, love, care, fun, angry];
}

module.exports = () => facebookScrapper()
