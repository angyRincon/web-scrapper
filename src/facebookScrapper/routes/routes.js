const express = require("express")
const router = express.Router();
const fbController = require("../controllers/controller")

router.get('/facebook-data', (req, res) => fbController.getFacebookData(req, res))
router.get('/facebook-emotions', (req, res) => fbController.getFacebookEmotions(req, res))


module.exports = router
