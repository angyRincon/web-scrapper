const Facebook = require("../models/Facebook")

const getFacebookData = async (req, res) => {
    const result = await Facebook.find()
    return res.json(result)
}

const getFacebookEmotions = async (req, res) => {
    const result = await Facebook.findOne({}, {}, {sort: {'createdAt': -1}})
    return res.json(result)
}

module.exports = {
    getFacebookEmotions,
    getFacebookData
}
