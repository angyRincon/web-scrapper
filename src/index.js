const express = require("express")
const mongoose = require("mongoose")
const app = express();
const instaScrap = require("./instagramScrapper/instagramScrap")
const faceScrap = require("./facebookScrapper/facebookScrapper")
const whatScrap = require("./whatsappScrapper/whatsappScrapper")
const fbRouter = require("./facebookScrapper/routes/routes")
const instRouter = require("./instagramScrapper/router/router")

mongoose.connect('mongodb://localhost/scrapp', {useNewUrlParser: true, useUnifiedTopology: true})
    .then(db => console.log('Connected to DB', db.connection.name))
    .catch(err => console.log(err));

// instaScrap();
faceScrap();
// whatScrap()

app.use('/', [
    fbRouter,
    instRouter
])

app.listen(3250, () => console.log("Listening on port", 3250))
