const mongoose = require("mongoose");

const Comments = mongoose.Schema({
    name: String,
    comments: Number,
    positiveComments: Number,
    negativeComments: Number,
    likes: {type: String},
    views: {type: String},
}, {timestamps: true, versionKey: false})

module.exports = mongoose.model('Comments', Comments)
