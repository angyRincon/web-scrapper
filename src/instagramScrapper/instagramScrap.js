const puppeteer = require("puppeteer")
const actions = require("./actions")
const chalk = require("chalk")

async function instaScrap() {
    try {
        const browser = await puppeteer.launch({
            headless: false,
            args: ["--disable-setuid-sandbox"],
            'ignoreHTTPSErrors': true
        })

        const page = await browser.newPage();

        // Login
        await actions.loginAction(page)
        await actions.submitAction(page)

        // Search
        await actions.searchAction(page)

        // Select post
        await actions.selectPost(page)

        // Get Name
        const name = await page.$eval("._7UhW9.fKFbl.yUEEX.KV-D4.fDxYl", txt => txt.textContent)

        /* // Next Page
         await page.waitForSelector("._65Bje.coreSpriteRightPaginationArrow", {visible: true})
         await Promise.all([
             page.waitForNavigation({waitUntil: "networkidle2"}),
             page.click("._65Bje.coreSpriteRightPaginationArrow"),
         ])*/

        // Filter comments
        let elements = await actions.filterComments.filterComments(page)
        const cleanElements = elements.map(element => element.replace(/[.!*#%@`´]/g, " "))
        console.log(cleanElements)

        // Filter by Positive Comments
        const positiveResult = await actions.filterComments.filterByPositive(cleanElements)
        console.log(positiveResult)

        // Filter by Negative Comments
        const negativeResult = await actions.filterComments.filterByNegative(cleanElements)
        console.log(negativeResult)

        // Filter by Likes and Views
        const likes = await page.$eval(".zV_Nj", txt => txt.textContent)
        /*const views = await page.$eval(".vcOH2", txt => txt.textContent)*/
        /*const views = data.includes('reproducciones') ? data.split(' ', 1) : ''
        const likes = data.includes('Me gusta') ? data.split(' ', 1) : ''*/
        console.log("VIEWS ", 0)
        console.log("LIKES ", likes)

        //Open Modal
        await Promise.all([
            page.waitForTimeout(2000),
            page.click(".zV_Nj")
        ])

        // Get urls
        const url = await page.$eval(".FPmhX.notranslate.MBL3Z", txt => txt.href)
        await page.goto(url)
        console.log(url)


        /*    // Save into database
            const newComments = new Comments({
                name: name,
                comments: cleanElements.length,
                positiveComments: positiveResult.length,
                negativeComments: negativeResult.length,
                views: views.toString(),
                likes: likes.toString()
            });

            await newComments.save();*/

        // Paginate
        await page.waitForSelector(".dCJp8.afkep", {visible: true})
        await Promise.all([
            page.waitForTimeout(3000),
            page.click(".dCJp8.afkep"),
        ])
    } catch (e) {
        console.error(chalk.red(e))
    }
}

module.exports = () => instaScrap()
