const express = require("express")
const router = express.Router();
const instController = require("../controllers/controller")

router.get('/instagram-data', (req, res) => instController.getInstagramData(req, res))
router.get('/instagram-emotion', (req, res) => instController.getInstagramEmotions(req, res))

module.exports = router
