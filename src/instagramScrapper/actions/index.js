const loginAction = require('./login/loginAction')
const submitAction = require('../../general')
const searchAction = require('./search/searchAction')
const selectPost = require('./posts/posts')
const filterComments = require('./comments/comments')


module.exports = {
    loginAction,
    submitAction,
    searchAction,
    selectPost,
    filterComments,
}

