const positiveWords = require("../../../words/positiveWords");
const negativeWords = require("../../../words/negativeWords");

const filterComments = async (page) => {
    await page.waitForSelector(".XQXOT.pXf-y", {visible: true})
    return await page.$$eval('.C4VMK > span', (a) => {
        return a.map(tx => tx.textContent)
    })
}

const filterByPositive = async (cleanElements) => {
    const filterByPositive = cleanElements.map(word => {
        const positive = positiveWords.map(positive => {
            const result = word.includes(positive)
            const value = result ? 'positvas' : 'negativas'
            if (value === 'positvas') return value
        })
        return positive.find(a => a !== undefined)
    })

    return filterByPositive.filter(a => a === 'positvas')
}

const filterByNegative = async (cleanElements) => {
    const filterByNegative = cleanElements.map(word => {
        const negative = negativeWords.map(negative => {
            const result = word.includes(negative)
            const value = result ? 'negativas' : 'positivas'
            if (value === 'negativas') return value
        })
        return negative.find(a => a !== undefined)
    })

    return filterByNegative.filter(a => a === 'negativas')
}

module.exports = {
    filterComments,
    filterByPositive,
    filterByNegative
}
