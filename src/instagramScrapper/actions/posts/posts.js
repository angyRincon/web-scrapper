const selectPost = async (page) => {
    await page.waitForSelector("._9AhH0", {visible: true});
    await Promise.all([
        page.waitForNavigation({waitUntil: "networkidle2"}),
        page.click("._9AhH0"),
    ]);
}

module.exports = (page) => selectPost(page)
