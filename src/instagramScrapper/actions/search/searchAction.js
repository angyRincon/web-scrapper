const searchAction = async (page) => {
    await page.waitForSelector("input[placeholder='Buscar']", {visible: true})
    await page.type("input[placeholder='Buscar']", "claudialopezcl");
    await page.waitForSelector(".eGOV_.ybXk5._4EzTm.XfCBB.HVWg4", {visible: true});
    await Promise.all([
        page.waitForNavigation({waitUntil: "networkidle2"}),
        page.click(".eGOV_.ybXk5._4EzTm.XfCBB.HVWg4"),
    ]);
} //martha.muvdi

module.exports = (page) => searchAction(page)
