const Instagram = require("../data-access/models/Comments")

const getInstagramData = async (req, res) => {
    const result = await Instagram.find()
    return res.json(result)
}

const getInstagramEmotions = async (req, res) => {
    const result = await Instagram.findOne({}, {}, {sort: {'createdAt': -1}})
    return res.json(result)
}

module.exports = {
    getInstagramEmotions,
    getInstagramData
}
