const submitAction = async (page) => {
    await Promise.all([
        page.waitForNavigation({waitUntil: "networkidle2"}),
        page.click("button[type='submit']"),
    ]);
}

module.exports = (page) => submitAction(page)
